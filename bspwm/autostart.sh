#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

$HOME/.config/polybar/launch.sh &
$HOME/.config/polybar1/launch1.sh &
$HOME/.config/polybar2/launch2.sh &
$HOME/.config/polybar3/launch3.sh &
$HOME/.config/polybar4/launch4.sh &

xsetroot -cursor_name left_ptr &
run sxhkd -c ~/.config/bspwm/sxhkd/sxhkdrc &

run xfce4-power-manager & 
run volumeicon &
run variety &
numlockx on &
playerctl &
imwheel -b 45 &
flameshot &
#guake &
picom --config $HOME/.config/bspwm/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
#nitrogen --restore &

