;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;	Themes : https://github.com/jaagr/dots/tree/master/.local/etc/themer/themes
;   https://github.com/jaagr/polybar/wiki/
;   https://github.com/jaagr/polybar/wiki/Configuration
;   https://github.com/jaagr/polybar/wiki/Formatting
;
;=====================================================

[global/wm]
;https://github.com/jaagr/polybar/wiki/Configuration#global-wm-settings
margin-top = 0
margin-bottom = 0

[settings]
;https://github.com/jaagr/polybar/wiki/Configuration#application-settings
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
screenchange-reload = true
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground = #FF0000
format-background = #00FF00
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

[colors]
; Nord theme ============
background = #981b1e31
foreground = #ffffff
alert = #d08770
volume-min = #a3be8c
volume-med = #ebcb8b
volume-max = #bf616a
; =======================

; Gotham theme ==========
; background = #0a0f14
; foreground = #99d1ce
; alert = #d26937
; volume-min = #2aa889
; volume-med = #edb443
; volume-max = #c23127
; =======================

; INTRCPTR theme ============
;background = ${xrdb:color0:#222}
;background = #aa000000
;background-alt = #444
;foreground = ${xrdb:color7:#222}
;foreground = #fff
;foreground-alt = #555
;primary = #ffb52a
;secondary = #e60053
;alert = #bd2c40


################################################################################
################################################################################
############                  MAINBAR-BSPWM                         ############
################################################################################
################################################################################

[bar/mainbar-bspwm]
monitor = DisplayPort-0
;monitor-fallback = HDMI1
width = 7.2%
height = 33
offset-x = 7.5%
offset-y = 0.3%
radius = 1
fixed-center = true
bottom = false
#separator = |

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

wm-restack = bspwm
override-redirect = true

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

border-size = 0
;border-left-size = 0
;border-right-size = 25
;border-top-size = 0
;border-bottom-size = 25
border-color = #00000000

padding-left = 3
padding-right = 1

module-margin-left = 1
module-margin-right = 9

;https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "SourceHanSansKR-Bold:size=11;0"
font-1 = "FontAwesome:size=11;0"
font-2 = "SourceHanSansKR-Bold:size=11;0"
font-3 = "SourceHanSansKR-Bold:size=11;0"

modules-left = xwindow
modules-center = 
modules-right = 

#tray-detached = false
#tray-offset-x = 0
#tray-offset-y = 0
#tray-padding = 2
#tray-maxsize = 20
#tray-scale = 1.0
#tray-position = right
#tray-background = ${colors.background}

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

; Locale used to localize various module data (e.g. date)
; Expects a valid libc locale, for example: fi_FI.UTF-8
locale = fi_FI.UTF-8


################################################################################
###############################################################################
############                       MODULES A-Z                      ############
################################################################################
################################################################################

[module/xwindow]
;https://github.com/jaagr/polybar/wiki/Module:-xwindow
type = internal/xwindow

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 25

format-foreground = ${colors.foreground}
format-background = ${colors.background}
